/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.util.ArrayList;

/**
 *
 * @author BmWz
 */
public interface DaoInterface<T>{
    public int add(T oblect);
    public ArrayList<T> getAll();
    public T get(int id);
    public int delete(int id);
    public int update(T object);
}
